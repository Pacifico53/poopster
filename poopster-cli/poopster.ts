import * as fs from 'fs';

interface Message {
    date: Date;
    sender: string;
    text: string;
    isPoop: boolean;
}

interface PersonStats {
    totalPoops: number,
    poopiestDay: string | null,
    poopiestTime: string | null,
    nonPoopMessages: number
    longestPoopBreak: string
}

interface ConversationStats {
    [pooper: string]: PersonStats,
}

interface CommonTimes {
    [pooper: string]: {
        days: string[],
        hours: string[],
        lastDate: Date | null
        poopBreakMs: number | null
    }
}

function parseChatConversation(conversation: string): Message[] {
    const messages: Message[] = [];
    
    const lines = conversation.split('\n');

    for (const line of lines) {
        const match = line.match(/^(\d+\/\d+\/\d+, \d+:\d+) - ([^:]+): (.*)$/);
        const nonMessageMatch = line.match(/^(\d+\/\d+\/\d+, \d+:\d+) - ([\w ]+)(added|pinned a message)/);

        if (nonMessageMatch) {
            continue;
        }

        if (match) {
            const [, dateTime, senderName, text] = match;
            const [date, time] = dateTime.split(', ');
            const [hour, minutes] = time.split(':');

            const msgDate = new Date(date)
            msgDate.setHours(parseInt(hour))
            msgDate.setMinutes(parseInt(minutes))

            messages.push({
                date: msgDate,
                sender: senderName,
                text,
                isPoop: text.includes('💩') && text.length < 3
            });
        } else if (messages.length > 0) {            
            // Line is continuation of previous message
            messages[messages.length - 1].text += `\n${line}`;
        }
    }

    return messages;
}

function readConversationFromFile(filePath: string): string {
    try {
        return fs.readFileSync(filePath, 'utf-8');
    } catch (error) {
        console.error(`Error reading the file: ${error}\nExport WhatsApp chat to Chat/`);
        process.exit(1);
    }
}

function findMostCommonString(strings: string[]): string | null {
    if (strings.length === 0) {
        return null;
    }

    const stringCounts: Record<string, number> = {};

    // Count occurrences of each string
    for (const str of strings) {
        stringCounts[str] = (stringCounts[str] || 0) + 1;
    }

    let mostCommonString: string = strings[0];
    let highestCount: number = stringCounts[mostCommonString] || 0;

    // Find the string with the highest count
    for (const str in stringCounts) {
        if (stringCounts[str] > highestCount) {
            mostCommonString = str;
            highestCount = stringCounts[str];
        }
    }

    return `${mostCommonString} (${highestCount} poops)`;
}

function getPoopStats(messages: Message[]) : ConversationStats {
    const convoStats: ConversationStats = {}
    const timeStats: CommonTimes = {}

    messages.forEach(msg => {
        const sender = msg.sender;

        if(!convoStats[sender]){
            convoStats[sender] = {
                totalPoops: 0,
                poopiestDay: '',
                poopiestTime: '',
                nonPoopMessages: 0,
                longestPoopBreak: ''
            }
        }
    
        if (!timeStats[sender]) {
            timeStats[sender] = {
                hours: [],
                days: [],
                lastDate: null,
                poopBreakMs: 0
            }
        }
        
        if (msg.isPoop) {
            convoStats[sender].totalPoops += 1
            if (timeStats[sender].lastDate) {
                var timeDiff = msg.date.getTime() - timeStats[sender].lastDate!.getTime();
                if (timeStats[sender].poopBreakMs! < timeDiff) {
                    const timeDhours = (timeDiff/(3600*1000))

                    convoStats[sender].longestPoopBreak = timeDhours <= 24 ?  timeDhours.toFixed(2) + ' hours.' : (timeDhours/24).toFixed(2) + ' days.'
                    timeStats[sender].poopBreakMs = timeDiff
                }                
            }
            timeStats[sender].lastDate = msg.date

            const hour= msg.date.getHours().toString()            
            timeStats[sender].hours.push(hour)

            const day = msg.date.toLocaleDateString('en-GB');
            timeStats[sender].days.push(day)            
        }
        else{
            convoStats[sender].nonPoopMessages += 1
        }
    })

    // Sort conversationStats by poops
    const sortedStats = Object.entries(convoStats)
        .sort(([, statsA], [, statsB]) => statsB.totalPoops - statsA.totalPoops)
        .reduce((obj, [key, value]) => {
            value.poopiestTime = findMostCommonString(timeStats[key].hours)
            value.poopiestDay = findMostCommonString(timeStats[key].days)

            obj[key] = value;
            return obj;
        }, {} as ConversationStats);
    

    return sortedStats
}

const filePath = './Chat/WhatsApp Chat with Count my poop.txt';
const conversationFile = readConversationFromFile(filePath);
const messages = parseChatConversation(conversationFile);
const fullStats = getPoopStats(messages)

console.log(fullStats)
