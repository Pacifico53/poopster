import React from 'react';
import { Bar, BarChart, CartesianGrid, Legend, Tooltip, XAxis, YAxis } from 'recharts';


interface HourlyPoopDistributionProps {
  hourlyData: {
    hour: number;
    poops: number;
  }[]
}

const HourlyPoopDistribution: React.FC<HourlyPoopDistributionProps> = ({ hourlyData }) => {
  return (
    <div>
      <h2>Hourly Distribution</h2>
      <div className="chart-container">
        <BarChart
          width={1100}
          height={300}
          data={hourlyData}
          margin={{ top: 5, right: 30, left: 20, bottom: 10 }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="hour" angle={-90} textAnchor="end" interval={0} />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="poops" fill="#8884d8" />
        </BarChart>
      </div>
    </div>
  );
};

export default HourlyPoopDistribution;
