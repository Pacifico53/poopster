import React from 'react';
import { CommonTimes } from '../../scripts/getPoopData';
import { BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar, ResponsiveContainer } from 'recharts';

interface Data {
  [day: number]: { person: string; poops: number }[],
}

interface PoopsPerDayProps {
  timeStats: CommonTimes
}

const PoopsPerDay: React.FC<PoopsPerDayProps> = ({ timeStats }) => {
  const data: Data = {}

  const parseDate = (input: string) => {
    var parts = input.match(/(\d+)/g);

    return new Date(parseInt(parts![2]), parseInt(parts![1]) - 1, parseInt(parts![0]));
  }

  const iterateThroughMonthDays = (year: number, month: number) => {
    const daysInMonth = new Date(year, month, 0).getDate();
    for (let day = 1; day <= daysInMonth; day++) {
      const currentDate = new Date(year, month - 1, day);
      data[day] = []

      Object.entries(timeStats).forEach(([pooper, stats]) => {
        const checkedDates: string[] = []
        stats.days.forEach(d => {
          const statdate = parseDate(d)
          if (statdate.getFullYear() === currentDate.getFullYear() &&
            statdate.getMonth() === currentDate.getMonth() &&
            statdate.getDate() === currentDate.getDate() &&
            !checkedDates.includes(d)) {
            data[day].push({
              person: pooper,
              poops: stats.days.filter((v) => (v === d)).length
            })
            checkedDates.push(d)
          }
        });
      });
    }
  }

  iterateThroughMonthDays(2024, 2);

  const chartData: { [day: string]: { [poops: string]: number } } = {};

  Object.keys(data).forEach(day => {
    data[parseInt(day)].forEach(entry => {
      if (!chartData[day]) {
        chartData[day] = {};
      }
      chartData[day][entry.person] = entry.poops;

    });
  });

  const chartDataArray = Object.keys(chartData).map(day => ({
    ...chartData[day]
  }));
  const persons: string[] = [];

  chartDataArray.forEach(group => {
    Object.keys(group).forEach(key => {
      if (!persons.includes(key)) {
        persons.push(key);
      }
    });
  });

  return (
    <div>
      <h2>Poops per Day</h2>
      <div className="chart-container">
        <ResponsiveContainer width="100%" height={800}>
          <BarChart
            data={chartDataArray}
            margin={{ top: 20, right: 30, left: 20, bottom: 10 }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="" tickFormatter={(value, index) => (index + 1).toString()} />
            <YAxis />
            <Tooltip />
            <Legend />
            {persons.map((pooper, index) => (
              <Bar key={index} dataKey={pooper} stackId="a" fill={`#${Math.floor(Math.random() * 16777215).toString(16)}`} />
            ))}
          </BarChart>
        </ResponsiveContainer>

      </div>
    </div>
  );
};

export default PoopsPerDay;
