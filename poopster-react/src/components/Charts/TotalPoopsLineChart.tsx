import React from 'react';
import { CartesianGrid, Line, LineChart, Tooltip, XAxis, YAxis } from 'recharts';


interface TotalPoopsLineChartProps {
  data: {
    name: string;
    poops: number;
  }[]
}

const TotalPoopsLineChart: React.FC<TotalPoopsLineChartProps> = ({ data }) => {
  return (
    <div>
      <h2>Total Poops</h2>
      <div className="chart-container">

        <LineChart
          width={1000}
          height={500}
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 200 }}
        >
          <XAxis dataKey="name" angle={-90} textAnchor="end" interval={0} />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Line type="monotone" dataKey="poops" stroke="#8884d8" activeDot={{ r: 8 }} />
        </LineChart>
      </div>
    </div>
  );
};

export default TotalPoopsLineChart;
