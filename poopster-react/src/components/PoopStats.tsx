import React from 'react';
import { CommonTimes, ConversationStats } from '../scripts/getPoopData';
import TotalPoopsLineChart from './Charts/TotalPoopsLineChart';
import HourlyPoopDistribution from './Charts/HourlyPoopDistribution';
import PoopsPerDay from './Charts/PoopsPerDay';

interface PoopStatsProps {
  convoStats: ConversationStats
  timeStats: CommonTimes
  onSelectPooper: (pooper: string) => void;
}

const PoopStats: React.FC<PoopStatsProps> = ({ convoStats, timeStats, onSelectPooper }) => {
  const totalPoopsData = Object.entries(convoStats).map(([pooper, stats]) => ({
    name: pooper,
    poops: stats.totalPoops,
  }));

  const hourlyData: { hour: number; poops: number; }[] = [];
  for (let h = 0; h <= 23; h++) {
    hourlyData.push({ hour: h, poops: 0 });
    Object.entries(timeStats).forEach(([pooper, stats]) => {
      stats.hours.forEach(el => {
        if (parseInt(el) === h) {
          hourlyData[h].poops += 1;
        }
      });
    });
  }

  return (
    <div>
      <h2>Select Pooper</h2>
      <div className="pooper-select">
        {Object.keys(convoStats).map(pooper => (
          <button key={pooper} onClick={() => onSelectPooper(pooper)}>
            {pooper}
          </button>
        ))}
      </div>

      <TotalPoopsLineChart data={totalPoopsData} />
      <HourlyPoopDistribution hourlyData={hourlyData} />
      <PoopsPerDay timeStats={timeStats}/>
    </div>
  );
};

export default PoopStats;
