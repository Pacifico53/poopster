import React from 'react';
import { PersonStats } from '../scripts/getPoopData';


interface PooperStatsProps {
  pooper: string;
  stats: PersonStats;
  onBack: () => void; // Callback function to navigate back
}

const PooperStats: React.FC<PooperStatsProps> = ({ pooper, stats, onBack }) => {
  return (
    <div>
      <h2>Stats for {pooper}</h2>
      <p>Total Poops: {stats.totalPoops}</p>
      <p>Poopiest Day: {stats.poopiestDay || 'N/A'}</p>
      <p>Poopiest Time: {stats.poopiestTime || 'N/A'}</p>
      <p>Non-Poop Messages: {stats.nonPoopMessages}</p>
      <p>Longest Poop Break: {stats.longestPoopBreak}</p>
      <button className='upload-button' onClick={onBack}>Back</button>
    </div>
  );
};

export default PooperStats;
