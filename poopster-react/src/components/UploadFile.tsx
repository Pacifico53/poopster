import React, { ChangeEvent, useRef, useState } from 'react';
import { CommonTimes, ConversationStats, poopsterBegin } from '../scripts/getPoopData';
import PoopStats from './PoopStats';
import PooperStats from './PooperStats';

const FileInput: React.FC = () => {
  const [convoStats, setConvoStats] = useState<ConversationStats | null>(null); // State to store uploaded file
  const [timeStats, setTimeStats] = useState<CommonTimes | null>(null);
  const [selectedPooper, setSelectedPooper] = useState<string | null>(null);

  const handleSelectPooper = (pooper: string) => {
    setSelectedPooper(pooper);
  };

  const handleBack = () => {
    setSelectedPooper(null);
  };

  // Create a reference to the hidden file input element
  const hiddenFileInput = useRef<HTMLInputElement>(null); // Add type annotation

  // Programatically click the hidden file input element
  // when the Button component is clicked
  const handleClick = () => {
    if (hiddenFileInput.current) {
      hiddenFileInput.current.click();
    }
  };

  const handleFileInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    const fileList = event.target.files;
    if (!fileList) return;

    const file = fileList[0];
    const reader = new FileReader();

    reader.onload = (event) => {
      if (event.target?.result) {
        const fileContent = event.target.result as string;
        const [stats, commontimes] = poopsterBegin(fileContent)
        setConvoStats(stats)
        setTimeStats(commontimes)
      }
    };

    reader.readAsText(file);
  };

  return (
    <div>
      {convoStats && timeStats ? (
        selectedPooper ? (
          <PooperStats
            pooper={selectedPooper}
            stats={convoStats[selectedPooper]}
            onBack={handleBack}
          />
        ) : (
          <PoopStats convoStats={convoStats} timeStats={timeStats} onSelectPooper={handleSelectPooper} />
        )
      ) : (
        <div>
          <button className="upload-button" onClick={handleClick}>
            Upload Chat
          </button>
          <input
            type="file"
            onChange={handleFileInputChange}
            accept=".txt" // You can specify the file types you want to accept here
            ref={hiddenFileInput}
            style={{ display: 'none' }} // Make the file input element invisible
          />
        </div>
      )}
    </div>
  );
};

export default FileInput;
