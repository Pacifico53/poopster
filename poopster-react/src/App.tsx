import React from 'react';
import poo from './poo.png'
import './App.css';
import FileInput from './components/UploadFile';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={poo} className="App-logo" alt="logo" />
        <p>
          Welcome to <b>Poopster</b><br/>
          The best way to analyse all the poops of your WhatsApp group chat.
        </p>
        <FileInput/>
      </header>
    </div>
  );
}

export default App;
